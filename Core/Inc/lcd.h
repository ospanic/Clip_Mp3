/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LCD_H
#define __LCD_H

/* Includes ------------------------------------------------------------------*/
#include "main.h"

#define CHINESS_FONT_ADDR 0x08032000
#define _DEF_FONT_CH     (const u8*)(CHINESS_FONT_ADDR)
	
typedef int32_t  s32;
typedef int16_t s16;
typedef int8_t  s8;
typedef __IO uint32_t  vu32;
typedef __IO uint16_t vu16;
typedef __IO uint8_t  vu8;
typedef uint32_t  u32;
typedef uint16_t u16;
typedef uint8_t  u8;
typedef const uint32_t uc32;  /*!< Read Only */
typedef const uint16_t uc16;  /*!< Read Only */
typedef const uint8_t uc8;   /*!< Read Only */

void LCD_Init(void);

void LCD_Write_Cmd(u8 cmd);
void LCD_Write_Data(u8 data);
void LCD_Set_Pos(u8 x, u8 y);
void LCD_Clean(u8 data);
int LCD_Disp_Str(u8 x, u8 y, u8 *data, u8 color);
void LCD_Disp_Txt(u8 *data);
void LCD_Disp_Number(u8 x, u8 y, u8 *data);
void LCD_Disp_Battery(u8 value);
void LCD_Disp_SideBar(u8 value);
void LCD_Disp_Icon(u8 x, u8 y, u8* icon, u8 len);
void LCD_Disp_ProgBar(u8 y, u8 value);
#endif /* __LCD_H */

/******************* (C) COPYRIGHT 2008 STMicroelectronics *****END OF FILE****/
