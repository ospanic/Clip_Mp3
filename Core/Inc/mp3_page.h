#ifndef __MP3_PAGE_H
#define __MP3_PAGE_H

/* Includes ------------------------------------------------------------------*/
#include "main.h"

int load_mp3_file_list(void);
void mp3_file_page_loop(void);
void mp3_play_page_loop(void);
#endif /* __MP3_PAGE_H */
