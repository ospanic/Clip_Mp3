#ifndef __GAME_PAGE_H
#define __GAME_PAGE_H

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include "main.h"

void game_page_loop(void);

#endif /* __GAME_PAGE_H */
