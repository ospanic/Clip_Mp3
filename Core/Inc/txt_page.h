#ifndef __TXT_PAGE_H
#define __TXT_PAGE_H

/* Includes ------------------------------------------------------------------*/
#include "main.h"

int load_txt_file_list(void);
void txt_file_page_loop(void);
void txt_read_page_loop(void);
#endif /* __MP3_PAGE_H */
