#ifndef __MP3_PLAY_H_
#define __MP3_PLAY_H_

int mp3play(char* filename);
void mp3_play_loop(void);
void mp3pause(void);
void mp3resume(void);
void mp3stop(void);
int mp3state(void);

int mp3get_total_time(char *p);
int mp3get_play_time(char *p);
int mp3get_play_progress(void);
#endif
