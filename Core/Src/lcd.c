#include "lcd.h"
#include "font.h"

extern SPI_HandleTypeDef hspi1;


void LCD_SPI_Write(u8 RS, u8 data)
{
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, RS?GPIO_PIN_SET:GPIO_PIN_RESET);//RS
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_RESET);//CS
	HAL_SPI_Transmit(&hspi1, &data, 1, 0xFF);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_SET);//CS
}

void LCD_Write_Cmd(u8 cmd)
{
	LCD_SPI_Write(0, cmd);
}

void LCD_Write_Data(u8 data)
{
	LCD_SPI_Write(1, data);
}

void LCD_Set_Pos(u8 x, u8 y)
{
	LCD_Write_Cmd(0x10+((x&0x70)>>4));
	LCD_Write_Cmd(x&0x0F);
	LCD_Write_Cmd(0xB0+y);
}
void LCD_Clean(u8 data)
{
	u8 y =0,x = 0;

	for(y=0;y<8;y++)
	{
		LCD_Write_Cmd(0x10);
		LCD_Write_Cmd(0x00);
		LCD_Write_Cmd(0xB0+y);
		
		for(x=0;x<128;x++)
		{
			LCD_Write_Data(data);
		}
	}
}

void LCD_Char_EN(u8 x, u8 y, u8 c, u8 color)
{
	int i = 0, d = (c - ' ') * 16;
	LCD_Write_Cmd(0x10+((x&0x70)>>4));
	LCD_Write_Cmd(x&0x0F);
	LCD_Write_Cmd(0xB0+y);
	
	for(i=0; i < 8; i++)
	{
		if(color > 0)  LCD_Write_Data(~font[d++]);
		else LCD_Write_Data(font[d++]);
	}
	
	LCD_Write_Cmd(0x10+((x&0x70)>>4));
	LCD_Write_Cmd(x&0x0F);
	LCD_Write_Cmd(0xB1+y);
	
	for(i=0; i < 8; i++)
	{
		if(color > 0)  LCD_Write_Data(~font[d++]);
		else LCD_Write_Data(font[d++]);
	}
}

void LCD_Char_HZ(u8 x, u8 y, u8 *data, u8 color)
{
	int i = 0;

	long offset;
	int quCode = 0;
	int weiCode = 0;
	
	quCode = data[0] - 0xa0;//
	weiCode = data[1] - 0xa0;//  
	offset = (94 * (quCode - 1) + (weiCode - 1)) * 32;
	
	const u8* addr = _DEF_FONT_CH + offset;
	
	LCD_Write_Cmd(0x10+((x&0x70)>>4));
	LCD_Write_Cmd(x&0x0F);
	LCD_Write_Cmd(0xB0+y);
	
	for(i=0; i < 16; i++)
	{
		if(color > 0)  LCD_Write_Data(~addr[i]);
		else LCD_Write_Data(addr[i]);
	}

	LCD_Write_Cmd(0x10+((x&0x70)>>4));
	LCD_Write_Cmd(x&0x0F);
	LCD_Write_Cmd(0xB1+y);
	
	for(i=16; i < 32; i++)
	{
		if(color > 0)  LCD_Write_Data(~addr[i]);
		else LCD_Write_Data(addr[i]);
	}
}

int LCD_Disp_Str(u8 x, u8 y, u8 *data, u8 color) // normal color = 0, Inverted color > 0
{
	u8 c = 0;
	while(data[0] > 0 && x < 128)
	{
		if(data[0] > 0x80) // HZ
		{
			if(x > 112) break;
			LCD_Char_HZ(x, y, data, color);
			data += 2;
			c += 2;
			x += 16;
		}
		else
		{
			LCD_Char_EN(x, y, data[0], color);
			data ++;
			c ++;
			x += 8;
		}
	}
	
	if(color == 0) return c;
	
	LCD_Write_Cmd(0x10+((x&0x70)>>4));
	LCD_Write_Cmd(x&0x0F);
	LCD_Write_Cmd(0xB0+y);
	
	for(u8 i = x; i < color; i ++)
	{
		LCD_Write_Data(0xFF);
	}
	
	LCD_Write_Cmd(0x10+((x&0x70)>>4));
	LCD_Write_Cmd(x&0x0F);
	LCD_Write_Cmd(0xB1+y);
	
	for(u8 i = x; i < color; i ++)
	{
		LCD_Write_Data(0xFF);
	}
	return c;
}

void LCD_Disp_Txt(u8 *data)
{
	u8 c = LCD_Disp_Str(0,0, data, 0);
	c += LCD_Disp_Str(0,2, data + c, 0);
	c += LCD_Disp_Str(0,4, data + c, 0);
	c += LCD_Disp_Str(0,6, data + c, 0);
}

void LCD_Disp_Number(u8 x, u8 y, u8 *data) // just for 0123456789:/
{
	LCD_Write_Cmd(0x10+((x&0x70)>>4));
	LCD_Write_Cmd(x&0x0F);
	LCD_Write_Cmd(0xB0+y);
	
	while(data[0] > 0)
	{
		if(data[0] > 0x2E || data[0] < 0x3B)
		{
			x = data[0] - 0x2F;
			LCD_Write_Data(font_number[x][0]);
			LCD_Write_Data(font_number[x][1]);
			LCD_Write_Data(font_number[x][2]);
			LCD_Write_Data(font_number[x][3]);
			LCD_Write_Data(font_number[x][4]);
			LCD_Write_Data(font_number[x][5]);
		}			
		data ++;
	}
}

void LCD_Disp_Battery(u8 value)
{
	u8 x = 111;
	LCD_Write_Cmd(0x10+((x&0x70)>>4));
	LCD_Write_Cmd(x&0x0F);
	LCD_Write_Cmd(0xB0);

	LCD_Write_Data(0x3C);
	LCD_Write_Data(0x24);
	LCD_Write_Data(0x66);
	LCD_Write_Data(0x81);
	x = 10 - value;
	
	while(x--)
	{
		LCD_Write_Data(0x81);
	}
	x = value;
	
	while(x--)
	{
		LCD_Write_Data(0xBD);
	}
	
	LCD_Write_Data(0x81);
	LCD_Write_Data(0x7E);
}

const u8 SideBar[7][7] = 
{
	{0xF8, 0x04, 0x04, 0x04, 0x04, 0x04, 0xF8},
	{0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff},
	{0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff},
	{0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff},
	{0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff},
	{0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff},
	{0x7F, 0x80, 0x80, 0x80, 0x80, 0x80, 0x7f}
};

u8 SideBar_buff[7][7] = { 0 };

void LCD_Disp_SideBar(u8 value)
{
	u8 y = 0, x = 120;
	
	for(x = 0; x < 7; x ++) // copy data
		for(y = 0; y < 7; y++)
			SideBar_buff[x][y] = SideBar[x][y];
	
	if(value > 42) value = 42;
	
	value += 4;
	
	if(value > 53) value = 53;
	
	y = (value / 8);
	SideBar_buff[y][2] |= (0xFF << (value %8));
	SideBar_buff[y][3] |= (0xFF << (value %8));
	SideBar_buff[y][4] |= (0xFF << (value %8));
	
	y ++ ;
	SideBar_buff[y][2] |= (0xFF >> (8 - (value %8)));
	SideBar_buff[y][3] |= (0xFF >> (8 - (value %8)));
	SideBar_buff[y][4] |= (0xFF >> (8 - (value %8)));
	
	for(y = 0; y < 8; y ++)
	{
		x = 120;
		LCD_Write_Cmd(0x10+((x&0x70)>>4));
		LCD_Write_Cmd(x&0x0F);
		LCD_Write_Cmd(0xB0 + y + 1);
		
		LCD_Write_Data(SideBar_buff[y][0]);
		LCD_Write_Data(SideBar_buff[y][1]);
		LCD_Write_Data(SideBar_buff[y][2]);
		LCD_Write_Data(SideBar_buff[y][3]);
		LCD_Write_Data(SideBar_buff[y][4]);
		LCD_Write_Data(SideBar_buff[y][5]);
		LCD_Write_Data(SideBar_buff[y][6]);
	}
}

void LCD_Disp_ProgBar(u8 y, u8 value)
{
	u8 x = 3;
	
	LCD_Write_Cmd(0x10+((x&0x70)>>4));
	LCD_Write_Cmd(x&0x0F);
	LCD_Write_Cmd(0xB0 + y);

	LCD_Write_Data(0x08);
	LCD_Write_Data(0x14);
	LCD_Write_Data(0x22);
	
	for(y = 0; y < value; y ++)
	{
		LCD_Write_Data(0x2A);
	}
	
	value = 116 - value;
	for(y = 0; y < value; y ++)
	{
		LCD_Write_Data(0x22);
	}
	LCD_Write_Data(0x14);
	LCD_Write_Data(0x08);
}

void LCD_Disp_Icon(u8 x, u8 y, u8* icon, u8 len)
{
	LCD_Write_Cmd(0x10+((x&0x70)>>4));
	LCD_Write_Cmd(x&0x0F);
	LCD_Write_Cmd(0xB0 + y);
	
	for(int i = 0; i < len; i ++)
	{
		LCD_Write_Data(icon[i]);
	}
}

/*
	uC8230��Һ���������Ĵ�������
*/
void LCD_Init(void)
{	
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_SET);//RS
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_SET);//CS

	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_15, GPIO_PIN_SET);//RESET
	
	LCD_Write_Cmd(0xE2);
	LCD_Write_Cmd(0xA2);
	LCD_Write_Cmd(0xA0);
	LCD_Write_Cmd(0xC8);
	LCD_Write_Cmd(0x26);
	LCD_Write_Cmd(0x2F);
	LCD_Write_Cmd(0x40);
	LCD_Write_Cmd(0xA0);
	LCD_Write_Cmd(0xAF);
	LCD_Write_Cmd(0x81);
	LCD_Write_Cmd(0x08);
	
	LCD_Clean(0x0);
	
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_SET);// Back Light
}
