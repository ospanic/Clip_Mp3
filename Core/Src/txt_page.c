/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : mp3play.c
  * @brief          : text fila select page display and text read
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include "main.h"
#include "fatfs.h"
#include "mp3play.h"
#include "lcd.h"

#include "txt_page.h"

static uint32_t len = 0;
static FIL TextFile;
static uint8_t res = 0;

int txt_open(char* filename)
{
	if(f_open(&TextFile, (const TCHAR*)filename, FA_READ)!= FR_OK)
	{
		printf("Text Open File [%s] Fail...\r\n", filename);
		return -1;
	}
		
	res = f_size(&TextFile);
	printf("Text File Size: %d \r\n", res);

	return 0;
}

static u8 txt_file_buff[64][16] = { 0 };
static int txt_file_count = 0;
int load_txt_file_list(void)
{
	DIR     dir; 
	FRESULT  fr = f_opendir(&dir, (const TCHAR*)"txt");
	FILINFO fno; 
	
	if(fr == FR_OK)
	{
		for(;;)
		{
			fr = f_readdir(&dir, &fno);
			if (fr != FR_OK || fno.fname[0] == 0) break; 
			strcpy((char*)txt_file_buff[txt_file_count], fno.fname);
			printf("%s\r\n", fno.fname);
			txt_file_count ++;
			
			if(txt_file_count == 256) break;
		}
		f_closedir(&dir);
		return txt_file_count;
	}
	return -1;
}

static char txt_file_path_buff[32] = { 0 };
static int txt_file_disp_index = 0;
static int txt_file_select_index = 0;

void txt_file_page_loop(void)
{
	Battery_Refresh();
	switch(g_key_value)
	{
		case 2: // up
			txt_file_select_index --;
			if(txt_file_select_index == -1) txt_file_select_index = 0;
			break;
		case 3: // down
			txt_file_select_index ++;
			if(txt_file_select_index == txt_file_count) txt_file_select_index = txt_file_count - 1;
			break;
		case 4: // left
			g_main_while_loop = main_page_loop;
			g_key_value = 99;
			return;
		case 1: // ok
			sprintf(txt_file_path_buff, "txt/%s", (const char*)txt_file_buff[txt_file_select_index]);
			if(txt_open(txt_file_path_buff) == 0)
			{
				g_main_while_loop = txt_read_page_loop;
				g_key_value = 99;
				return;
			}
			break;
		case 99: // clean 
			LCD_Clean(0x00);
			break;
		default:
			return;
	}
	g_key_value = 0;
	if(txt_file_select_index - txt_file_disp_index > 3)
	{
		txt_file_disp_index = txt_file_select_index - 3;
	}		
	else if(txt_file_disp_index > txt_file_select_index)
	{
		txt_file_disp_index = txt_file_select_index;
	}
	LCD_Clean(0x00);
	for(int i = 0; i < 4; i ++)
	{
		if(txt_file_disp_index +i == txt_file_select_index)
		{
			LCD_Disp_Str(0, i * 2, txt_file_buff[txt_file_disp_index + i], 100);
		}
		else
		{
			LCD_Disp_Str(0, i * 2, txt_file_buff[txt_file_disp_index + i], 0);
		}
	}
	double p = (double)(txt_file_select_index) / (txt_file_count - 1);
	p = p *42;
	
	LCD_Disp_SideBar((u8)p);
}

static char txt_read_buff[128] = { 0 };
void txt_read_page_loop(void)
{
	switch(g_key_value)
	{
		case 2: // up,return file page
			g_main_while_loop = txt_file_page_loop;
			g_key_value = 99;
			break;
		case 3: // down, no action
			res = f_read(&TextFile, txt_read_buff, 64, &len);
			break;
		case 4: // lift, paly proc
			res = f_lseek(&TextFile, f_tell(&TextFile) - 128);
			res = f_read(&TextFile, txt_read_buff, 64, &len);
			break;
		case 5: // right, paly next
			break;
		case 1: // ok, pause, play
			break;
		case 99: // ok
			LCD_Clean(0x00);
			res = f_read(&TextFile, txt_read_buff, 64, &len);
			break;
		default:
			return;
	}
	g_key_value = 0;
	LCD_Disp_Txt((u8*)txt_read_buff);
}

