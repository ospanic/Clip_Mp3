/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : game_page.c
  * @brief          : select game
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include "main.h"
#include "fatfs.h"
#include "lcd.h"
#include "mp3play.h"
#include "game_dino.h"
#include "game_tetris.h"

#define GAME_NUM 3

static int game_index = 0x00;
void game_page_loop(void)
{
	Battery_Refresh();
	switch(g_key_value)
	{
		case 2: // up
			g_main_while_loop = main_page_loop;
			g_key_value = 99;
			return;
		case 3: // down
			return;
		case 4: // left
			game_index ++;
			break;
		case 5: // right
			game_index --;
			break;
		case 1: // ok
			if(game_index == 0) 
			{	
				Dino_Init();
				g_main_while_loop = game_dino_loop;
			}
			else if(game_index ==1) 
			{
				Tetris_Init();
				g_main_while_loop = game_tetris_loop;
			}
			g_key_value = 99;
			return;
		case 99: // clean 
			LCD_Clean(0x00);
			break;
		default:
			return;
	}
	g_key_value = 0;
	if(game_index == GAME_NUM) game_index = 0;
	if(game_index == -1) game_index = GAME_NUM - 1;
	
	if(game_index == 0)
	{
		LCD_Disp_Str(16, 2, "<   Doni   >", 0);
	}
	else if(game_index == 1)
	{
		LCD_Disp_Str(16, 2, "<  Tetris  >", 0);
	}
	else if(game_index == 2)
	{
		LCD_Disp_Str(16, 2, "<   Snack  >", 0);
	}
}
