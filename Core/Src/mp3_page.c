/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : mp3_page.c
  * @brief          : select mp3 file
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include "main.h"
#include "fatfs.h"
#include "mp3play.h"
#include "lcd.h"

#include "mp3_page.h"

const uint8_t icon_play[] = { 0x00, 0x10, 0x38, 0x7C, 0xFE, 0x00 };
const uint8_t icon_pause[]= { 0xFE, 0xFE, 0x00, 0x00, 0xFE, 0xFE };
const uint8_t icon_return[]={ 0x04, 0x0E, 0x1F, 0x04, 0x44, 0x6C, 0x38 };
const uint8_t icon_prev[] = { 0xFE, 0x10, 0x38, 0x7C, 0xFE, 0x10, 0x38, 0x7C, 0xFE };
const uint8_t icon_next[] = { 0xFE, 0x7C, 0x38, 0x10, 0xFE, 0x7C, 0x38, 0x10, 0xFE };

u8 mp3_file_buff[64][16] = { 0 };
int mp3_file_count = 0;
int load_mp3_file_list(void)
{
	DIR     dir; 
	FRESULT  fr = f_opendir(&dir, (const TCHAR*)"mp3");
	FILINFO fno; 
	
	if(fr == FR_OK)
	{
		for(;;)
		{
			fr = f_readdir(&dir, &fno);
			if (fr != FR_OK || fno.fname[0] == 0) break; 
			strcpy((char*)mp3_file_buff[mp3_file_count], fno.fname);
			printf("%s\r\n", fno.fname);
			mp3_file_count ++;
			
			if(mp3_file_count == 256) break;
		}
		f_closedir(&dir);
		return mp3_file_count;
	}
	return -1;
}

static char mp3_file_path_buff[32] = { 0 };
static int mp3_file_disp_index = 0;
static int mp3_file_select_index = 0;
static int mp3_file_play_index = 0;

void mp3_file_page_loop(void)
{
	Battery_Refresh();
	switch(g_key_value)
	{
		case 2: // up
			mp3_file_select_index --;
			if(mp3_file_select_index == -1) mp3_file_select_index = 0;
			break;
		case 3: // down
			mp3_file_select_index ++;
			if(mp3_file_select_index == mp3_file_count) mp3_file_select_index = mp3_file_count - 1;
			break;
		case 4: // lift, return main page
			g_main_while_loop = main_page_loop;
			g_key_value = 99;
			return;
		case 5: // right, enter play page
			g_main_while_loop = mp3_play_page_loop;
			g_key_value = 99;
			return;
		case 1: // ok
			mp3stop();
			sprintf(mp3_file_path_buff, "mp3/%s", (const char*)mp3_file_buff[mp3_file_select_index]);
			if(mp3play(mp3_file_path_buff) == 0)
			{
				g_main_while_loop = mp3_play_page_loop;
				mp3_file_play_index = mp3_file_select_index;
				g_key_value = 99;
				return;
			}
			break;
		case 99: // ok
			LCD_Clean(0x00);
			break;
		default:
			return;
	}
	g_key_value = 0;
	if(mp3_file_select_index - mp3_file_disp_index > 3)
	{
		mp3_file_disp_index = mp3_file_select_index - 3;
	}		
	else if(mp3_file_disp_index > mp3_file_select_index)
	{
		mp3_file_disp_index = mp3_file_select_index;
	}
	LCD_Clean(0x00);
	for(int i = 0; i < 4; i ++)
	{
		if(mp3_file_disp_index +i == mp3_file_select_index)
		{
			LCD_Disp_Str(0, i * 2, mp3_file_buff[mp3_file_disp_index + i], 100);
		}
		else
		{
			LCD_Disp_Str(0, i * 2, mp3_file_buff[mp3_file_disp_index + i], 0);
		}
	}
	double p = (double)(mp3_file_select_index) / (mp3_file_count - 1);
	p = p *42;
	
	LCD_Disp_SideBar((u8)p);
}

void mp3_play_page_loop(void)
{
	static uint32_t last_refresh = 0;
	switch(g_key_value)
	{
		case 2: // up,return file page
			g_main_while_loop = mp3_file_page_loop;
			g_key_value = 99;
			return;
		case 3: // down, no action
			break;
		case 4: // lift, paly proc
			if(mp3_file_play_index == 0) break;
			mp3stop();
			mp3_file_play_index --;
			sprintf(mp3_file_path_buff, "mp3/%s", (const char*)mp3_file_buff[mp3_file_play_index]);
			mp3play(mp3_file_path_buff);
			g_key_value =99;
			return;
		case 5: // right, paly next
			if(mp3_file_play_index == mp3_file_count - 1) break;
			mp3stop();
			mp3_file_play_index ++;
			sprintf(mp3_file_path_buff, "mp3/%s", (const char*)mp3_file_buff[mp3_file_play_index]);
			mp3play(mp3_file_path_buff);
			g_key_value =99;
			return;
		case 1: // ok, pause, play
			if(mp3state() == 1) mp3pause();
			else mp3resume();
			g_key_value =99;
			return;
		case 99: // ok
			LCD_Clean(0x00);
			sprintf(mp3_file_path_buff, "%d/%d", mp3_file_play_index, mp3_file_count);
			LCD_Disp_Number(3, 0, (u8*)mp3_file_path_buff);
			LCD_Disp_Str(16, 2, mp3_file_buff[mp3_file_play_index], 0); // file name
			LCD_Disp_Icon(60, 0, (u8*)icon_return, sizeof(icon_return)); // return icon
			LCD_Disp_Icon(3, 7, (u8*)icon_prev, sizeof(icon_prev)); // last icon
			LCD_Disp_Icon(115, 7, (u8*)icon_next, sizeof(icon_next)); // next icon
			if(mp3state() == 1) LCD_Disp_Icon(62, 7, (u8*)icon_pause, sizeof(icon_pause));
			else LCD_Disp_Icon(62, 7, (u8*)icon_play, sizeof(icon_play));
			break;
		default:
			if((HAL_GetTick() - last_refresh) > 1000) break;
			else return;
	}
	
	last_refresh = HAL_GetTick();
	g_key_value = 0;

	mp3get_play_time(mp3_file_path_buff);
	LCD_Disp_Number(3, 5, (u8*)mp3_file_path_buff);
	
	mp3get_total_time(mp3_file_path_buff);
	LCD_Disp_Number(94,5, (u8*)mp3_file_path_buff);
	
	LCD_Disp_ProgBar(6,  mp3get_play_progress());	
	printf("Refresh\r\n");
	
}
